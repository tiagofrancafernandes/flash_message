<?php
/**
 * Flash message
 * Mensagens
 */

$theme = <<<EOF
<div style="color:red;">
Begin of theme<br>
{{MESSAGE}}<br>
End of theme
</div>
EOF;

if (!session_id()) {
	session_start();
}

function setMessage($msg) {
	$_SESSION['msg'] = $msg;
}

function removeMessage() {
	if (isset($_SESSION['msg'])) {
		unset($_SESSION['msg']);
	}
}

function showMessage($showTheme = false) {
	if (isset($_SESSION['msg'])) {
		global $theme;
		if ($showTheme === true) {
			$search = "{{MESSAGE}}";
			$msg = $_SESSION['msg'];
			$res = str_replace($search, $msg, $theme);
			echo $res;
			removeMessage();
		} else {
			echo $_SESSION['msg'] . "<br>";
			removeMessage();
		}
	}
}

?>